<?php

namespace Drupal\uw_cbl_facts_and_figures\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\image\Entity\ImageStyle;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblFactsFiguresPreprocessBlock.
 */
class UwCblFactsFiguresPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with CTAs and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_facts_and_figures')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content');

      // The counter used for the number of FF text.
      $text_details_count = 0;

      // Get the classes from variables.
      $classes = $variables->get('attributes')['class'];

      // If there are classes in the attributes, and check for carousel settings.
      if (isset($classes)) {

        // These are the classes to check for.
        $classes_to_check = [
          'uw-carousel--one-item',
          'uw-carousel--two-items',
          'uw-carousel--three-items',
          'uw-carousel--four-items',
        ];

        // Step through each of the classes in attributes and see if we have
        // any carousel settings and set a variable if we do.
        foreach ($classes as $class) {
          if (in_array($class, $classes_to_check)) {
            switch ($class) {
              case 'uw-carousel--one-item':
                $num_per_carousel = 1;
                break;
              case 'uw-carousel--two-items':
                $num_per_carousel = 2;
                break;
              case 'uw-carousel--three-items':
                $num_per_carousel = 3;
                break;
              case 'uw-carousel--four-items':
                $num_per_carousel = 4;
                break;
              default:
                $num_per_carousel = 3;
                break;
            }
          }
        }
      }

      // Get the FF blocks from the content variable.
      $fact_figure = $block['field_uw_ff_fact_figure'][0]['#paragraph'];

      // The uuid of the paragraph to use as a unique identifier.
      $ffs['id'] = $fact_figure->uuid();

      // The background color for the FF.
      $ffs['bg_colour'] = $fact_figure->get('field_uw_ff_dbg_color')->getValue() ? $fact_figure->get('field_uw_ff_dbg_color')->getValue()[0]['value'] : NULL;

      // The default colour of the FF.
      $ffs['theme'] = $fact_figure->get('field_uw_ff_def_color')->getValue() ? $fact_figure->get('field_uw_ff_def_color')->getValue()[0]['value'] : NULL;

      // The show bubbles of the FF.
      $ffs['show'] = $fact_figure->get('field_uw_ff_show_bubbles')->getValue() ? $fact_figure->get('field_uw_ff_show_bubbles')->getValue()[0]['value'] : NULL;

      // The text align of the FF.
      $ffs['text_align'] = $fact_figure->get('field_uw_ff_textalign')->getValue()[0]['value'] ? $fact_figure->get('field_uw_ff_textalign')->getValue()[0]['value'] : NULL;

      // The number of FFs to show in the carousel.
      $ffs['num_per_carousel'] = $num_per_carousel;

      // Get the details about the FF.
      $ff_details = $fact_figure->get('field_uw_fact_figure')->getValue();

      // Step through each detail of the FF and get the content.
      foreach ($ff_details as $ff_detail) {

        // Check if the text details has a target_id.
        if (isset($ff_detail['target_id']) && $ff_detail['target_id']) {

          // If there is an existing text details it will have a target_id,
          // and we need to load in the paragraph.
          $entity = \Drupal\paragraphs\Entity\Paragraph::load($ff_detail['target_id']);
        }
        else {

          // If it is a new text details we need to load in the entity.
          $entity = $ff_detail['entity'];
        }

        // Get the actual FF info.
        $ff_info = $entity->get('field_uw_ff_info')->getValue();

        // Step through each info and get out all the text details.
        foreach ($ff_info as $info) {

          // Check if the text details has a target_id.
          if (isset($info['target_id']) && $info['target_id']) {

            // Load in the entity.
            $info_entity = \Drupal\paragraphs\Entity\Paragraph::load($info['target_id']);
          } else {

            // Use the entity in the variables.
            $info_entity = $info['entity'];
          }

          // Get the type of text (big, medium, small, icon).
          $text_type = $info_entity->get('field_uw_ff_type_of_text')->getValue()[0]['value'];

          // If the text type is icon, load icon, if not use type of text.
          if ($text_type == 'icon') {

            // Retrieve the file entity using the target_id from the image field.
            $file = \Drupal\file\Entity\File::load($info_entity->get('field_uw_ff_icon')->getValue()[0]['target_id']);

            // Load in the image style that we are going to use.
            $style = ImageStyle::load('uw_is_icon');

            // Set the text details with the icon.
            $ffs_details[$text_details_count][] = [
              // Build the URL using the image style and file uri.
              'icon' => $style->buildUrl($file->getFileUri()),
              'type' => $text_type,
            ];
          } else {

            // Get the values of the text.
            $ffs_details[$text_details_count][] = [
              'text' => $info_entity->get('field_uw_ff_text')->getValue()[0]['value'],
              'type' => $text_type,
            ];
          }
        }

        // Increment the text details counter.
        $text_details_count++;
      }
    }

    // If there are ff details add the the ff detail variable.
    if (isset($ffs_details)) {
      $ffs['details'] = $ffs_details;
    }

    // If there are CTAs, assign them to the variables for the template to use.
    if (isset($ffs)) {
      $variables->set('ffs', $ffs);
    }
  }
}
