<?php

namespace Drupal\uw_cbl_facts_and_figures\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\field_event_dispatcher\Event\Field\WidgetFormAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblFactsFiguresAlterForm.
 */
class UwCblFactsFiguresAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    // Get the form from the event.
    $form = &$event->getForm();

    if ($this->checkLayoutBuilder($event, 'Facts and Figures')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for blockquote.
      $form['#validate'][] = [$this, 'uw_cbl_facts_and_figures_validation'];
    }
  }

  /**
   * Form validation for blockquote.
   */
  public function uw_cbl_facts_and_figures_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        if ($block['field_uw_ff_fact_figure'][0]['subform']['field_uw_fact_figure']) {

          $ffs_text = $block['field_uw_ff_fact_figure'][0]['subform']['field_uw_fact_figure'];

          foreach ($ffs_text as $text_key => $ff_text) {
            if (is_int($text_key) && isset($ff_text['subform'])) {
              // TO DO: figure out why can get the values when the paragraph is closed.
            }
          }
        }
      }
    }
  }

  /**
   * Alter widget form for FFs to add states to type of text.
   *
   * @param \Drupal\field_event_dispatcher\Event\Field\WidgetFormAlterEvent $event
   *   The event.
   */
  public function alterWidgetForm(WidgetFormAlterEvent $event): void {

    // Get the context from the event.
    $context = $event->getContext();

    /** @var \Drupal\field\Entity\FieldConfig $field_definition */
    $field_definition = $context['items']->getFieldDefinition();
    $paragraph_entity_reference_field_name = $field_definition->getName();

    if ($paragraph_entity_reference_field_name == 'field_uw_ff_info') {

      // Get the element from the event.
      $element = &$event->getElement();

      // Get the form state from the event.
      $form_state = &$event->getFormState();

      /** @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::formElement() */
      $widget_state = \Drupal\Core\Field\WidgetBase::getWidgetState($element['#field_parents'], $paragraph_entity_reference_field_name, $form_state);

      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph_instance = $widget_state['paragraphs'][$element['#delta']]['entity'];
      $paragraph_type = $paragraph_instance->bundle();

      // Determine which paragraph type is being embedded.
      if ($paragraph_type == 'uw_para_fact_text') {

        // Variable used for selector.
        $selector = '';

        // Step through each of the parents to get the selector.
        for ($i = 0; $i < count($element["subform"]["field_uw_ff_text"]["#parents"]) - 1; $i++) {

          // Get the parent from the subform element.
          $parent = $element["subform"]["field_uw_ff_text"]["#parents"][$i];

          // If the parent is in an integer, add.
          if (is_int($parent)) {
            $selector .= '[' . $parent . ']';
          }
          // If the parent is settings, add it without the brackets.
          else if ($parent == 'settings') {
            $selector .= 'settings';
          }
          // All the rest of the parents add with brackets.
          else {
            $selector .= '[' . $element["subform"]["field_uw_ff_text"]["#parents"][$i] . ']';
          }
        }

        // If there is a selector add on the field name.
        if (isset($selector) && $selector !== '') {
          $selector = 'select[name="' . $selector . '[field_uw_ff_type_of_text]"]';
        }

        // Dependent fields.
        $element['subform']['field_uw_ff_text']['#states'] = [
          'visible' => [
            $selector => [
              ['value' => 'big'],
              ['value' => 'medium'],
              ['value' => 'small']
            ]
          ],
        ];

        // Dependent fields.
        $element['subform']['field_uw_ff_icon']['#states'] = [
          'visible' => [
            $selector => [
              ['value' => 'icon'],
            ]
          ],
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
      HookEventDispatcherInterface::WIDGET_FORM_ALTER => 'alterWidgetForm',
    ];
  }
}
